Systematic photo monitoring
----------------------------
Actions
**********
Load all images - clears the list, loads and show all images in the database
Load unidentified images
Clear list - clears the list
Upload new images

Event date
***********
When appending individual record to a set of photos, event_date and event_time are the minimum values in the set.
