.. _modules:

Modules
---------------
The data in the mBase2 database are organized into the modules. There are currently two modules implemented:

-  Systematic photo monitoring
-  Signs of presence

All the modules have common admin interface where you can:

-  set properties (grid cell size and color when displayed on a map)
-  set variables of the module
-  set variables for all the referenced tables of the module

Module properties
*******************

.. image:: ./images/ksnip_20201102-012205.png
   :width: 600

Module variables
******************
Each variable has to have its data type defined. There are two data types which represent reference to another code list or table, i.e. `code_list_reference` and `table_reference`.

.. image:: ./images/ksnip_20201102-012216.png
   :width: 600

Referenced tables
*****************
The same holds for the tables which are referenced by the module. Each of them has to have the variables defined. The referenced table can be selected from 
the list of all directly and indirectly referenced tables of the selected module.

.. image:: ./images/ksnip_20201102-012303.png
   :width: 600

Systematic photo monitoring
***************************
The module data is organized as follows:

.. image:: ./images/Diagram1.png
   :width: 600

SESSION is identified by start and optionally end date of photos taken on the TRAP_STATION_ID with CAMERA_ID under some SURVEY_ID.