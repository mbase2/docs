Batch imports
--------------

The principle of batch import follows the way of adding a single record, except that in the batch import data is not entered via input form but read from the uploaded file. However, you still have to define a mapping between the data source (ie. file from which you are importing the data) and the variables. Moreover, you also have to upload all the images which you refer to in the data source file. You also have to define all the code lists and referenced tables data before the import - the row is rejected when importing the data if the system doesn't find a code list key.

.. warning::
   **Before the batch import**
   - make sure that the code lists and referenced tables data meet the entries in your data source file
   - upload all the files (images) which are refered in the data source file
  