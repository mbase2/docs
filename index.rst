.. mBase2 documentation master file, created by
   sphinx-quickstart on Mon Oct 12 00:52:04 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to mBase2's documentation!
==================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   useraccess
   codelists
   modules
   importingthedata
   exportingthedata
   queryingthedata
   
