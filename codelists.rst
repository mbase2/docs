Code lists
----------

More or less every referable item in the database is managed by the code list. Code list record has only one required field which represents key and has to be unique
in the respective code list (we have more code lists and each code list has more values). Some keys represent database column names and they have to adhere to the rules
of the database column names while other can be arbitrarely long and can have any UTF-8 character present. There rules are governed by the user interface.

.. image:: ./images/ksnip_20201102-012114.png
   :width: 600

Code list values can be imported individually or in batches - like every other user table in the mBase2 system.

.. image:: ./images/ksnip_20201102-012143.png
   :width: 600