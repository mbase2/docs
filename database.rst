Every module has a set of general variables, these are: _species_name, _licence_name, _even_date and _location. These are reserved words and cannot be used as keys for
the new variables. Additionally every module has a primary key column _id which also cannot be used as a key for the new variable.

How to add referenced table?
2. Insert table names as key in the referenced_tables code_list
3. Make an entry in the mbase2.referenced_tables
4. Define variables for referenced table
5. update schema