Importing the data
------------------

Before importing the data you have to have your code lists and referenced tables organized (cf. :ref:`modules`)

Raw import
***********
This kind of import is governed solely by the table variables data types and the user entry form is the same for every data type in question. When importing raw data
you have to select the module or the referenced table (marked with the asterisk) to which you are importing the data.

.. image:: ./images/ksnip_20201102-012839.png
   :width: 600

For the selected module/table there is data present in a tabular form.

.. image:: ./images/ksnip_20201102-022545.png
    :width: 600

Then you have two possibilities:

-  indiviudal record
-  batch import

.. image:: ./images/ksnip_20201102-022305.png
   :width: 600

Add a record
....................
The form is generated according to the defined variables. Once the record is inserted it is of course also shown in the records table.

.. image:: ./images/ksnip_20201102-012516.png
   :width: 600

.. image:: ./images/ksnip_20201102-013551.png
   :width: 600

Batch import
...................
Here you have to upload the `xslx` or the `csv` column data file and define the column mapping.

.. image:: ./images/ksnip_20201102-012538.png
   :width: 600

.. image:: ./images/ksnip_20201028-001231.png
   :width: 600

.. image:: ./images/ksnip_20201102-014028.png
   :width: 600

Application modules
********************
Application module import essentially mimics the underlying data structure by handling imports to the related data tables but the result is the same - a record or records
in the database. For example you can modify the signs of presence records by handling raw data for every table of the module (for every different type of spotted sign) 
or via application where this is handled for you. Or in the `Systematic photo monitoring` where you can assign sighting to more images at once, etc.

Signs of presence
..................
The data entry is performed in two steps - general step and a step specific to the selected sign of presence.

.. image:: ./images/ksnip_20201112-023400.png
   :width: 600

The principle of data entry should be general as described above.

Systematic photo monitoring
............................
On the left handside there is a list of all the files upload by user:

-  user can clear the list and upload additional files. 
-  user can select one or more files from the list
-  user can add a record to the selected file(s)
-  one file can have more records (identifications/sightings)

.. image:: ./images/ksnip_20201102-014248.png
   :width: 600

On the right hand side there is last selected image shown with the zoom possibility and below is the table data for the records of this image - or when no image is selected for 
all the images in the left hand side list. User can also batch assign the data to the images.

Importing the data from Camelot
*********************************

Before the import
............................

.. warning::
   Importing data from Camelot heavily relies on the expected column names of the Camelot's full data export file. These are the column names that are to be present (not necessarily in this order):
   
   - "Sighting Updated"
   - "Survey ID"
   - "Survey Name"
   - "Camera ID"	
   - "Make"	
   - "Model"	
   - "Camera Name"
   - "Site ID"
   - "Site Name"
   - "Species Common Name"
   - "Trap Station ID"
   - "Trap Station Name"
   - "Session End Date"
   - "Trap Station Session ID"	
   - "Session Start Date"

   **Please check if full export from your Camelot instance in the first row contains the above mentioned column names.**

Before the import you also have to enter some data about your Camelot installation into the *camelot_sources* data table (http://173.212.213.157/mbase2/admin/camelot). The only required field is an URL on which Camelot
can be found and the name of the source - this is the name you are going to see in the *import-from-camelot.exe* application and also part of the ID field for Camelot sessions,
stations, surveys (this is needed because you can import the data from several Camelot sources).

.. image:: 2020-12-22-21-35-15.png
   :width: 600


Importing from Camelot can be done using a desktop application found at http://173.212.213.157/dist/import_from_camelot.zip.
In the unzipped file there is executable file *import-from-camelot.exe* - when you run it, you are (after login) presented with the following screen:

.. image:: ./images/ksnip_20201109-025627.png
   :width: 600

In the desktop application dropdown you should see the Camelot sources defined before in the web application in the camelot sources data table (http://173.212.213.157/mbase2/admin/camelot). All you have to do now in the desktop application is select
your Camelot instance definition from the dropdown and press the button *Import data to mbase2*. The process usually takes few minutes - 
depending on the amount of data to transfer and your internet connection speed. You should see progress indicator and notices about the status of the import process.

.. image:: 2020-12-22-22-02-36.png
   :width: 600

After the import there is an import report available here: http://173.212.213.157/mbase2/admin/reports/batches

.. image:: ./2020-12-09-14-48-12.png
   :width: 600

If there is a difference in the number of rows in the import data source and the number of imported rows you can obtain a detailed error report by clicking on the red number in the *# of imported rows / # all rows* column 
of the respective batch import row.

.. image:: ./2020-12-09-14-52-37.png
   :width: 600


