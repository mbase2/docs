Filtering and exporting the data
------------------
Below the table there should be CSV and COPY button. CSV exports the filtered (Search input box in the upper right corner of the table) table contents to a CSV file and COPY copies the content to the clipboard.

.. image:: ./images/ksnip_20201112-024222.png
    :width: 600
